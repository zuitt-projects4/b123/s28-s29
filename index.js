/*
	loading the express js module into our app and into the express var.
	express module willl allows us to use  expressjs methods to create our api

	create an application(app) with expressjs
		- this creates an expressjs methods to var app
	app is our server

	port is a variable to contain the port when we want to designate
	
	express has methods to use as routes corresponding to each HTTP method.
	get(<endpoints>, <funtion To Handle request and responses>)
		- once the route is accessed, we can send a response with the use of res.send()
		- res.semd() actually combines writeHead() and end() already.
		- it is used to send a response to the client and ios the end of the response. 

	express.json() - allows us to handle the request's body and automatically parse the incoming JSON to a JS Object we can access and manage.
*/
const express = require("express")

const app = express()

const port = 4000
app.use(express.json())

let users = [

	{
		email: "mighty12@gmail.com",
		username: "mightyMouse12",
		password: "notrelatedtomickey",
		isAdmin: false
	},
	{
		email: "minnieMouse@gmail.com",
		username: "minniexmickey",
		password: "minniesincethestart",
		isAdmin: false
	},
	{
		email: "mickeyTheMouse@gmail.com",
		username: "mickeyKing",
		password: "thefacethatrunstheplace",
		isAdmin: true
	}
]

let items = []

let loggedUser;

app.get('/', (req, res) => {res.send("Hello World!")})

app.get('/hello', (req, res) => {res.send("Hello from Batch 123!")})

app.post('/', (req, res) =>{

	console.log(req.body)

	res.send(`Hello, I'm ${req.body.name}. I am  ${req.body.age}. I could be described as ${req.body.description}`)
})
//register
app.post('/users', (req, res) =>{

	console.log(req.body)
	let newUser = {

		email:req.body.email,
		username: req.body.username,
		password:req.body.password,
		isAdmin: req.body.isAdmin

	}

	users.push(newUser)
	console.log(users)
	res.send("Registered Successfully.")

})

//login
app.post('/users/login', (req, res) =>{

	console.log(req.body)
	let foundUser = users.find((user)=>{

		return user.username === req.body.username && user.password === req.body.password

	})

	if(foundUser !== undefined){
		//get the index number of the foundUser, but since the users array is an array of objects we have to use findIndex() instead, will iterate over all of the items and return the index number of the current items that matches the return conditions. It is similar to find() but instead returns only the index number
		let foundUserIndex = users.findIndex((user)=>{
			return user.username === foundUser.username
		})
		//This will add the index of your found user in the foundUser obj
		foundUser.index = foundUserIndex

		//temporarily log user in. allows us to refer the details of a logged in user.
		loggedUser = foundUser
		console.log(loggedUser)
		res.send("Thank you for logging in.")

	}else{
		res.send("Login failed. Wrong Credentials.")
	}
})

//addItem
app.post('/items', (req, res) => {

	console.log(loggedUser)
	console.log(req.body)
	let item = req.body
	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		let newItem = {
			name: item.name,
			description: item.description,
			price: item.price,
			isActive: item.isActive
		}
		items.push(newItem)
		console.log(items)
		res.send("You have added a new item.")
	}else{
		res.send("Unauthorized: Action Forbidden")
	}

})

//getItems
app.get('/items', (req, res) =>{
	console.log(loggedUser)

	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		res.send(items)
	}else{
		res.send("Unauthorized: Action Forbidden")
	}
})
/*
	getSingleUser
		GET request should not have a request body.
		It may have headers for additional information or we can add a small amount of data somewhere else : the url
		app.get('/users/:index') -route params are values we can pass via the URL.
		this is done especially to allow us to send a small amount of data into our server.
		Route parameters can be defines in the endpoint of a route with (:parameterName)	
		req.params - is an object that contains the route params.
		its properties are then determined by your route parameters.
		terminal output({index:'<endpoint>'})
		how to access the actual route params.//string
		req.params.index being a part of the url string is a string, so we need to parse it as a proper integer.
 */

 app.get('/users/:index', (req, res) => {

 	console.log(req.params)
 	console.log(req.params.index)
 	let index = parseInt(req.params.index)
 	/*console.log(typeof index)*/
 	let user = user[index]
 	res.send(user)

 })

 /*
	updateUser

		We're going to update the password of our user. However we should get the user first.
		To do this we should not pass the index of the user in the body but instead in our route params.
		
		getting proper users from the array with our index("users[userIndex]")

		req.body.password - comes from the body of your request
 */

 app.put('/users/:index', (req, res) => {

 	console.log(req.params)
 	console.log(req.params.index)
 	let userIndex = parseInt(req.params.index)

 	if(loggedUser !== undefined && loggedUser.index === userIndex){

	 	users[userIndex].password = req.body.password
	 	console.log(users[userIndex])
	 	res.send('Users password has been updated.')

 	}else{

 		res.send('Unauthorized. Login the correct user first.')

 	}

 })

//getsingleItem
app.get('/items/getSingle/:index', (req, res) =>{

	console.log(req.params)
	console.log(req.params.index)
	let itemIndex = parseInt(req.params.index)
	res.send(items[itemIndex])

})

//archive items
app.put('/items/archive/:index', (req, res) =>{
	
	console.log(req.params)
	console.log(req.params.index)
	let itemIndex = parseInt(req.params.index)
	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		
		items[itemIndex].isActive = false
		res.send("Item Archived.")

	}else{

		res.send("Unauthorized: Action Forbidden")

	}

})

//activate items
app.put('/items/activate/:index', (req, res) =>{
	
	console.log(req.params)
	console.log(req.params.index)
	let itemIndex = parseInt(req.params.index)
	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		
		items[itemIndex].isActive = true
		res.send("Item Activated.")

	}else{

		res.send("Unauthorized: Action Forbidden")

	}

})

app.listen(port, () => console.log(`Server is running at port ${port}`))
